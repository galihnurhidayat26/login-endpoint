const express = require('express');
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const app = express();
const bcrypt = require('bcryptjs')


app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send("Hello");
});

app.post('/auth/login', (req, res) => {
    const path = 'postgres://galih:galih2983901831jahdjhs@68.183.189.214:6432/galih_db'
    const sequelize = new Sequelize(path, {
        operatorAliases: false,
        logging: false
    });

    let USERLOGIN = sequelize.define('USERLOGINs', {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        digest: Sequelize.STRING
    }, {
        freezeTableName: true
    });

    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    USERLOGIN.findOne({
        where: { email: req.body.email }
    }).then(user => {
        const hresult = bcrypt.compareSync(req.body.password, user.digest);
        res.json({
            "code": 200,
            "message": `Welcome ${user.name}`
        })
        console.log(hresult);
        console.log(user.get({ plain: true }));
    }).finally(() => {
        sequelize.close();
    });

    console.log(req.body.email);
    console.log(req.body.password);
});

app.listen(process.env.port || 3000);
console.log('Web Server is listening at port ' + (process.env.port || 3000));