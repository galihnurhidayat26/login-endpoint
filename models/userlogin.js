'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class USERLOGIN extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  USERLOGIN.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    digest: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'USERLOGIN',
  });
  return USERLOGIN;
};