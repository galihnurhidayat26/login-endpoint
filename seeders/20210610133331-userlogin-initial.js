'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('USERLOGINs', [{
      name: "galih nurhidayat",
      email: "galihnurhidayat26@gmail.com",
      digest: "$2y$12$S7XeZygTJ0uy0Ix5noQiUefUZl/l1QI37ZeH6E.FhpViY8egvK8B6",
      createdAt: "2021-06-07 21:33:58.224000 +00:00",
      updatedAt: "2021-06-07 21:33:58.224000 +00:00"
    }])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
